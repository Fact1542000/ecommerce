using Ecommerce.Api.Startup;
using Ecommerce.Infrastructure;

var builder = WebApplication.CreateBuilder(args);
{
    builder.Services.AddInfrastructureServices(builder.Configuration);
    builder.Services.AddApiServices();
}

var app = builder.Build();
{
    app.ConfigureSwagger(); 
    
    await app.SeedDataHandle();

    app.UseHttpsRedirection();

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
