namespace Ecommerce.APi.Dtos.Brand;

public record PostBrandDto(string Name, bool State);
