namespace Ecommerce.APi.Dtos.Brand;

public record PutBrandDto(string Name, bool State);
