namespace Ecommerce.Api.Dtos.Category;

public record PutCategoryDto(string Name, bool State);
