namespace Ecommerce.Api.Dtos.Category;

public record PostCategoryDto(string Name, bool State);
