namespace Ecommerce.Api.Dtos.Store;

public record PostStoreDto
(
    string Name,
    bool State
);
