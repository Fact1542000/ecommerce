namespace Ecommerce.Api.Dtos.Store;

public record PutStoreDto
(
    string Name,
    bool State
);
