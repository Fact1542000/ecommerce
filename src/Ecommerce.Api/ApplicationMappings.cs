using AutoMapper;
using Ecommerce.Api.Dtos.Category;
using Ecommerce.Api.Dtos.Product;
using Ecommerce.Api.Dtos.Store;
using Ecommerce.APi.Dtos.Brand;
using Ecommerce.Core.Entities;

namespace Ecommerce.Api;

public class ApplicationMappings : Profile
{
    public ApplicationMappings()
    {
        #region Product mappings
        CreateMap<Product, GetProductDto>();
        CreateMap<PostProductDto, Product>();
        CreateMap<PutProductDto, Product>();
        #endregion

#region Store mappings
        CreateMap<PostStoreDto, Store>();
        CreateMap<PutStoreDto, Store>();
#endregion

#region Brand mappings
        CreateMap<PostBrandDto, Brand>();
        CreateMap<PutBrandDto, Brand>();
#endregion

#region Category mappings
        CreateMap<PostCategoryDto, Category>();
        CreateMap<PutCategoryDto, Category>();
#endregion
    }
}
