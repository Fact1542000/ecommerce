using Ecommerce.Core.Consts;
using Ecommerce.Core.Interfaces;
using Ecommerce.Core.Models;
using Ecommerce.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Api.Controllers;

public class AuthenticateController : ApiControllerBase
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly ITokenService _tokenService;
    public AuthenticateController(
        UserManager<ApplicationUser> userManager,
        RoleManager<IdentityRole> roleManager,
        ITokenService tokenService)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _tokenService = tokenService;
    }

    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] RegisterUser model)
    {
        var userExist = await _userManager.FindByNameAsync(model.UserName);
        if (userExist != null)
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{ Status = "Error", Message = "User already exist"});

        ApplicationUser user = new()
        {
            Email = model.Email,
            SecurityStamp = Guid.NewGuid().ToString(),
            UserName = model.UserName
        };

        var result = await _userManager.CreateAsync(user, model.Password);
        if (!result.Succeeded)
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status="Error", Message="User creation failed! Please try again"});

        return Ok(new Response{Status = "Success", Message = "User created successfully"});
    }

    [HttpPost("register-admin")]
    public async Task<IActionResult> RegisterAdmin([FromBody] RegisterUser model)
    {
        var userExist = await _userManager.FindByNameAsync(model.UserName);
        if (userExist != null)
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{ Status = "Error", Message = "User already exist"});

        ApplicationUser user = new()
        {
            Email = model.Email,
            SecurityStamp = Guid.NewGuid().ToString(),
            UserName = model.UserName
        };

        var result = await _userManager.CreateAsync(user, model.Password);
        if (!result.Succeeded)
            return StatusCode(StatusCodes.Status500InternalServerError, new Response{Status="Error", Message="User creation failed! Please try again"});

        if (!await _roleManager.RoleExistsAsync(UserRoles.Admin))
        {
            await _roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
        }
        if (!await _roleManager.RoleExistsAsync(UserRoles.User))
        {
            await _roleManager.CreateAsync(new IdentityRole(UserRoles.User));
        }

        if (await _roleManager.RoleExistsAsync(UserRoles.Admin))
        {
            await _userManager.AddToRoleAsync(user, UserRoles.Admin);
        }

        return Ok(new Response{Status = "Success", Message = "User created successfully"});
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] LoginUser model)
    {
        var user = await _userManager.FindByEmailAsync(model.Email);
        if (user is not null && await _userManager.CheckPasswordAsync(user, model.Password))
        {
            var token = await _tokenService.CreateToken(model);

            return Ok(new {
                token,
                user.UserName
            });
        }
        return Unauthorized();
    }
}
