using AutoMapper;
using Ecommerce.Api.Dtos.Product;
using Ecommerce.Core.Consts;
using Ecommerce.Core.Entities;
using Ecommerce.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Api.Controllers;

[Authorize(Roles = UserRoles.Admin)]
public class ProductController : ApiControllerBase
{
    private readonly IProductService _productService;
    private readonly IEfRepository<Product> _productRepo;
    private readonly IMapper _mapper;

    public ProductController(
        IProductService productService,
        IMapper mapper,
        IEfRepository<Product> productRepo)
    {
        _productService = productService;
        _mapper = mapper;
        _productRepo = productRepo;
    }

    [HttpGet("GetAll")]
    public async Task<ActionResult<IEnumerable<GetProductDto>>> GetAllProducts()
    {
        var products = await _productRepo.GetAllAsync(IncludeProperty: "Brand,Category");
        var productsDto = products.Select(p => _mapper.Map<GetProductDto>(p));
        return productsDto.ToList();
    }

    [HttpGet("GetById/{id}", Name = "GetProductById")]
    public ActionResult<GetProductDto> GetProductById(int id)
    {
        if (id < 1)
            return BadRequest("Invalid id");

        var product = _productRepo.GetFirst(x => x.Id == id, IncludeProperty: "Brand,Category");

        if (product is null)
            return NotFound($"Product with the id::{id} not found");

        var productDto = _mapper.Map<GetProductDto>(product);

        return productDto;
    }

    [HttpPost("Create")]
    public async Task<IActionResult> CreateProduct([FromBody] PostProductDto productDto) 
    {
        if (!ModelState.IsValid)
            return BadRequest("Invalid product");

        var product = _mapper.Map<Product>(productDto);
        var productInserted = await _productRepo.AddAsync(product);
        
        if (productInserted is null)
            return BadRequest("Could not create the product");

        return RedirectToRoute(nameof(GetProductById), new { id = productInserted.Id});
    }

    [HttpPut("Edit/{id}")]
    public async Task<IActionResult> EditProduct(int id, [FromBody] PutProductDto productDto)
    {
        if (id < 1)
            return BadRequest("Invalid id");

        var productToEdit = _productRepo.GetFirst(x => x.Id == id);

        if (productToEdit is null)
            return NotFound($"Product with Id::{id} not found");

        productToEdit.Name = productDto.Name;
        productToEdit.Price = productDto.Price;
        productToEdit.CategoryId = productDto.CategoryId;
        productToEdit.BrandId = productDto.BrandId;

        _productRepo.UpdateAsync(id, productToEdit);

        var result = await _productRepo.SaveChangeAsync();

        if (result < 1)
            return BadRequest("Could not edit the product");

        return NoContent();
    }

    [HttpDelete("Delete/{id}")]
    public async Task<IActionResult> DeleteProduct(int id)
    {
        if (id < 1)
            return BadRequest("Invalid id");

        await _productRepo.RemoveAsync(id);

        var result = await _productRepo.SaveChangeAsync();

        if (result < 1)
            return BadRequest($"Could not delete the product with Id::{id}");

        return NoContent();
    }
}
