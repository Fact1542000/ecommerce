using AutoMapper;
using Ecommerce.Api.Dtos.Store;
using Ecommerce.Core.Consts;
using Ecommerce.Core.Entities;
using Ecommerce.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Api.Controllers;

[Authorize(Roles = UserRoles.Admin)]
public class StoreController : ApiControllerBase
{
    private readonly IStoreService _storeService;
    private readonly IEfRepository<Store> _storeRepo;
    private readonly IMapper _mapper;

    public StoreController(
        IStoreService storeService,
        IEfRepository<Store> storeRepo,
        IMapper mapper)
    {
        _storeService = storeService;
        _storeRepo = storeRepo;
        _mapper = mapper;
    }

    [HttpGet("GetAll")]
    public async Task<ActionResult<IEnumerable<Store>>> GetAllStores()
    {
        var stores = await _storeRepo.GetAllAsync();    
        return stores.ToList();
    }

    [HttpGet("GetById/{id}", Name = "GetStoreById")]
    public ActionResult<Store> GetStoreById(int id)
    {
        if (id < 1)
            return BadRequest("Invalid id");

        var store = _storeRepo.GetFirst(x => x.Id == id);

        if (store is null)
            return NotFound($"Could not found the store with the Id::{id}");

        return store;
    }

    [HttpPost("Create")]
    public async Task<IActionResult> CreateStore([FromBody] PostStoreDto storeDto)
    {
        if (!ModelState.IsValid)
            return BadRequest("Invalid store");

        var store = _mapper.Map<Store>(storeDto);

        Store storeCreated = await _storeRepo.AddAsync(store);

        if (storeCreated is null)
            return BadRequest("Could not create the store");

        return RedirectToRoute(nameof(GetStoreById), new { id = storeCreated.Id });
    }

    [HttpPut("Edit/{id}")]
    public async Task<IActionResult> EditStore(int id, PutStoreDto storeDto)
    {
        if (id < 1)
            return BadRequest("Invalid id");

        var storeToUpdate = _storeRepo.GetFirst(x => x.Id == id);

        if (storeToUpdate is null)
            return NotFound($"Could not found the store with the Id::{id}");

        storeToUpdate.Name = storeDto.Name;
        storeToUpdate.State = storeDto.State;
        
        _storeRepo.UpdateAsync(id, storeToUpdate);

        var result = await _storeRepo.SaveChangeAsync();

        if (result < 1)
            return BadRequest("Could not update the store");

        return NoContent();
    }

    [HttpDelete("Delete/{id}")]
    public async Task<IActionResult> DeleteStore(int id)
    {
        if (id < 1)
            return BadRequest("Invalid id");

        var storeToDelete = _storeRepo.GetFirst(x => x.Id == id);

        if (storeToDelete is null)
            return NotFound($"Could not found the store with the Id::{id}");

        _storeRepo.Remove(storeToDelete);

        var result = await _storeRepo.SaveChangeAsync();

        if (result < 1)
            return BadRequest("Could not delete the store");

        return NoContent();
    }
}
