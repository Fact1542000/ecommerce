using Ecommerce.Core.Entities;
using Ecommerce.Core.Interfaces;

namespace Ecommerce.Core.Services;

public class BasketService : IBasketService
{
    private readonly IEfRepository<Basket> _basketRepo;
    private readonly IEfRepository<Product> _productRepo;
    private readonly IEfRepository<Store> _storeRepo;

    public BasketService(IEfRepository<Store> storeRepo, IEfRepository<Product> productRepo, IEfRepository<Basket> basketRepo)
    {
        _storeRepo = storeRepo;
        _productRepo = productRepo;
        _basketRepo = basketRepo;
    }
}
