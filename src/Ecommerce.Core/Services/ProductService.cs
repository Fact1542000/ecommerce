using Ecommerce.Core.Entities;
using Ecommerce.Core.Interfaces;

namespace Ecommerce.Core.Services;

public class ProductService : IProductService
{
    private readonly IEfRepository<Product> _productRepo;
    private readonly IEfRepository<Store> _storeRepo;

    public ProductService(IEfRepository<Product> productRepo, IEfRepository<Store> storeRepo)
    {
        _productRepo = productRepo;
        _storeRepo = storeRepo;
    }
}
