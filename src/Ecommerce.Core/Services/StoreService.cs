using Ecommerce.Core.Entities;
using Ecommerce.Core.Interfaces;

namespace Ecommerce.Core.Services;

public class StoreService : IStoreService
{
    private readonly IEfRepository<Store> _storeRepo;
    private readonly IEfRepository<Product> _productRepo;

    public StoreService(IEfRepository<Store> storeRepo, IEfRepository<Product> productRepo)
    {
        _storeRepo = storeRepo;
        _productRepo = productRepo;
    }
}
