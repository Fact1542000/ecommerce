namespace Ecommerce.Core.Entities;

public class ProductStore : BaseEntity
{
    public int ProductId { get; set; }
    public Product Product { get; set; } = null!;
    public int StoreId { get; set; }
    public Store Store { get; set; } = null!;
}
