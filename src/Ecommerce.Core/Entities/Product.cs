namespace Ecommerce.Core.Entities;

public class Product : BaseEntity
{
    public string Name { get; set; } = null!;
    public float Price { get; set; }
    public int BrandId { get; set; }
    public Brand Brand { get; set; } = null!;
    public int CategoryId { get; set; }
    public Category Category { get; set; } = null!;

    public void ChangePrice(float newPrice)
    {
        Price = newPrice;
    }
}
