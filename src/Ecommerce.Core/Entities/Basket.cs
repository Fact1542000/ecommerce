namespace Ecommerce.Core.Entities;

public class Basket : BaseEntity
{
    public int ProductId { get; set; }
    public Product Product { get; set; } = null!;
    public int ApplicationUserId { get; set; }
    public int Quantity { get; set; }
    public float Total { get; set; }
    public bool IsEmpty { get; set; } = false;

    public Basket()
    {
        Total = Product.Price * Quantity;
    }

    public void AddMoreProductQuantity(int quantity = 1)
    {
        Quantity += quantity;
    }

    public void RemoveProductQuantity(int quantity = 1)
    {
        if (Quantity == 0)
        {
            IsEmpty = true;
            return;
        }

        Quantity -= quantity;
    }
}