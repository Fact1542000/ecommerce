using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Ecommerce.Infrastructure.Options;

public class JWTOPtionsSetup : IConfigureOptions<JWTOptions>
{
    private readonly IConfiguration _configuration;
    private const string ConfigurationSectionName = "JWT";

    public JWTOPtionsSetup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void Configure(JWTOptions options)
    {
        _configuration.GetSection(ConfigurationSectionName).Bind(options);
    }
}