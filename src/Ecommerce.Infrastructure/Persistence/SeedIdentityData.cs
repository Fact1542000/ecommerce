using Ecommerce.Core.Consts;
using Ecommerce.Infrastructure.Identity;
using Ecommerce.Infrastructure.Persistence.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Infrastructure.Persitence;

public static class SeedIdentityData
{
    const string defaultPassword = "Password.123";
    public static async Task Handle(
        ApplicationDbContext context,
        UserManager<ApplicationUser> userManager,
        RoleManager<IdentityRole> roleManager)
    {
        if (context.Database.IsSqlServer())
        {
            context.Database.Migrate();
        }

        await roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
        await roleManager.CreateAsync(new IdentityRole(UserRoles.User));

        var defaultUser = new ApplicationUser { UserName = "default", Email = "default@gmail.com" };
        await userManager.CreateAsync(defaultUser, defaultPassword);
        defaultUser = await userManager.FindByNameAsync("default");
        await userManager.AddToRoleAsync(defaultUser, UserRoles.User);

        var adminUser = new ApplicationUser { UserName = "admin", Email = "admin@gmail.com" };
        await userManager.CreateAsync(adminUser, defaultPassword);
        adminUser = await userManager.FindByNameAsync("admin");
        await userManager.AddToRoleAsync(adminUser, UserRoles.Admin);
    }
}
