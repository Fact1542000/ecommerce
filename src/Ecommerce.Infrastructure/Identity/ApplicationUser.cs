using Microsoft.AspNetCore.Identity;

namespace Ecommerce.Infrastructure.Identity;
public class ApplicationUser : IdentityUser
{}