# Sql
```
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=francis.123" -p 1433:1433 -d 615
```

# Migrations
```
dotnet ef migrations add AddEntities --project src/Ecommerce.Infrastructure --startup-project src/Ecommerce.Api --output-dir Persistence/Migrations
```

# Admin user
```
{
  "email": "admin@gmail.com",
  "password": "Password.123"
}
```
